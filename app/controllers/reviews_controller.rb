class ReviewsController < ApplicationController
  before_action :find_place
  
  def create
    if already_reviewed? 
      flash[:notice] = "You can't reviewed more than once"
    else
      @place.reviews.user_id = current_user.id
      @place.reviews.create(review_params)
    end
  end

  private

  def review_params
    params.require(:review).permit(:interior, :service, :food_quality, :comment, :user_id, :place_id)
  end

  def find_place
    @place = Place.find(params[:place_id])
  end

  def already_reviewed?
    Review.where(review_params).exists?
  end

end
