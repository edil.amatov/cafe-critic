class PlacesController < ApplicationController
  before_action :set_place, only: [:show]

  def index
    @places = Place.all
    # @categoried_places = Place.find(params[:category_id]) 
  end

  def new
    @place = Place.new
  end

  def create
    @place = Place.new(place_params)
    @place.user_id = current_user.id
    if @place.save
      flash[:notice] = "Ваше заведение находится на модерации и будет добавлено после одобрения администратором"
      redirect_to root_path
    else
      render 'new'
    end
  end

  def show
  end

  private

  def set_place
    @place = Place.find(params[:id])
  end

  def place_params
    params.require(:place).permit(:title, :category_id, :description, :main, :user_id, :pictures[])
  end

end
