class Place < ApplicationRecord
  has_many :reviews, dependent: :destroy
  has_many :users, through: :reviews 

  belongs_to :category
  belongs_to :user
  has_many_attached :pictures
  has_one_attached :main
end
