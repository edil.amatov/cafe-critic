class User < ApplicationRecord
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable, :trackable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable

  has_many :reviews, dependent: :destroy
  has_many :places, through: :reviews 

  has_many :places , dependent: :destroy
  has_many_attached :images
end
