Rails.application.routes.draw do
  post 'reviews/create'
  root 'places#index'
  resources :places
  devise_for :users
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  ActiveAdmin.routes(self)
    resources :places
    resources :users
    resources :reviews
    resources :categories
end
