class AddColumToReviews < ActiveRecord::Migration[5.2]
  def change
    add_column :reviews, :comment, :text
    add_column :reviews, :food_quality, :float
    add_column :reviews, :service, :float
    add_column :reviews, :interior, :float
  end
end
