# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

def copy_image_fixture(place, file)
  fixtures_path = Rails.root.join('app', 'assets', 'images', 'fixtures', "#{file}.jpeg")

  place.main.attach(io: File.open(fixtures_path), filename: "#{file}.jpeg")
end


user = User.create(
        email: 'admin@example.com',
        password: 'Passw0rd',
        password_confirmation: 'Passw0rd',
        is_admin: true
)

category = Category.create(
        title: 'Restaurant'
)

3.times do |i|
  place = Place.create(title: Faker::Restaurant.name, description: Faker::Restaurant.description, user_id: user.id, category_id: category.id )
  copy_image_fixture(place, i+1)  
  
  place.reviews.create(comment: "Lorem ipsum dolor sit amet.", food_quality: rand(1..5), service: rand(1..5), interior: rand(1..5), user_id: user.id, place_id: place.id )
  
end